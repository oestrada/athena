#include "TrigMinBias/TrigTrackCounter.h"
#include "TrigMinBias/TrigTrackCounterHypo.h"
#include "TrigMinBias/TrigVertexCounter.h"
#include "TrigMinBias/TrigVertexCounterHypo.h"

DECLARE_COMPONENT( TrigTrackCounter )
DECLARE_COMPONENT( TrigTrackCounterHypo )
DECLARE_COMPONENT( TrigVertexCounter )
DECLARE_COMPONENT( TrigVertexCounterHypo )

